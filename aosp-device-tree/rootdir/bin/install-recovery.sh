#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:134217728:6aa79d21e501580b980de41a53e1384b9d7d9781; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:07bdb0431144f6017322beaf5100d4e22dc0cac9 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:134217728:6aa79d21e501580b980de41a53e1384b9d7d9781 && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
